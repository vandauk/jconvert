## JCONVERT v.1.3.0

## Description
Simple cli-based Audio and Video Batch Converter. The Script can download Videos and Audios from several Websites like Youtube.
The main purpose is to convert multiple video files of an whole folder. 
The encoding settings were chosen so that it is possible to reduce the file size without visible loss of quality.

![Main Menu](images/main_menu.png)

## Dependencies
- yt-dlp
- ffmpeg
- ffmpeg-libfdk_aac (for AAC audio encoding)
- cuda (optional for NVENC Encoding)

## Installation

First install the yt-dlp and ffmpeg package with your default package manager.
Create git folder in your home directory:

`cd ~ && mkdir git && cd git`

clone the repository and navigate into it:

`git clone https://gitlab.com/vandauk/jconvert.git && cd jconvert`

create user bin folder if it does not exist already:

`mkdir -p ~/.local/bin`

link the runscript jconvert into the bin directory of your home directory to be able to start the application with the command jconvert in every directory:

`ln -s ~/git/jconvert/jconvert ~/.local/bin/jconvert`

if your shell does not detect the application, make sure that `/home/username/.local/bin` is part of the `$PATH` environment variable. A new directory can be added to a user’s `$PATH` by editing `~/.bash_profile` or `~/.bashrc` files in the user’s home directory. A alternative but not recommended possibility is to link the application into the system bin directory:

`sudo ln -s ~/git/jconvert/jconvert /bin/jconvert`


# Quick Start Guide

## Download Audio and Video

Open a Terminal and navigate into the folder you want to download the file in. Launch jconvert and add the video link as parameter

`jconvert https://youtu.be/CYOzaGQRcKU`

Press 1 or 2, to select the menu for Audio or Video download. Select the format bitrate and in case of an video also the resolution. After pressing the corresponding key, the download and conversion process will start automaticly.

## Batch Conversion of Audio and Video

Open a terminal and navigate into the folder with the video- oder audiofiles you want to convert. Launch jconvert and append the filetype you want to convert. If you want to convert for example all "*.mp4" Files in the corresponding folder:

`jconvert mp4`

Now press 3 or 4 to open the audio/video conversion menu:

![Video Conversion Menu](images/video_menu.png)

Select one of the presets or press 5 for custom settings and folow the instructions. The script will automaticly convert all selected files of the given file type in the current working directory.
If you want to convert all video files inside the current folder just launch the script without parameters. It will select automaticly all compatible video-files.

# NVIDIA Hardware Encoding

It is possible to use your Nvidia graphics card for video conversion by selecting the "HEVC_NVENC" Encoder. This will speed up the encoding, but at the price of a larger file size. It is necessary to install the "cuda" package with your preferred package manager.

# AAC Audio Encoding

The application supports the the Fraunhofer FDK AAC (libfdk_aac) audio codec in the video convertion module for better compatibility with playback devices like TVs, which do not support the more efficient opus-codec.
